//
//  TipsViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/8/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit

class TipsViewController: UIViewController {
    
    
    @IBOutlet weak var introButton: UIButton!
    @IBOutlet weak var paraghButton: UIButton!
    @IBOutlet weak var conclusionButton: UIButton!
    @IBOutlet weak var discussionButton: UIButton!
    @IBOutlet weak var opinionButton: UIButton!
    @IBOutlet weak var opinionDiscussButton: UIButton!
    @IBOutlet weak var situationButton: UIButton!
    
    var tipType : String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == false, animated: true);
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.selectedIndex = 2
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "left-angle-bracket.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(TopicsTableViewController.back(sender:)))
        // let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TopicsTableViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        
         introButton.layer.borderWidth = 1
         paraghButton.layer.borderWidth = 1
         conclusionButton.layer.borderWidth = 1
         discussionButton.layer.borderWidth = 1
         opinionButton.layer.borderWidth = 1
         opinionDiscussButton.layer.borderWidth = 1
         situationButton.layer.borderWidth = 1
 
    }
    
    @objc func back(sender: UIBarButtonItem) {
        tabBarController?.selectedIndex = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - button Pressed

    @IBAction func tipButtonsPressed(_ sender: AnyObject) {
        
        if sender.tag == 0 {
            tipType = "How to write introduction"
            print(tipType)
            performSegue(withIdentifier: "tipsDetails", sender: self)
        } else if sender.tag == 1 {
            tipType = "How to write paragraphs"
        } else if sender.tag == 2 {
            tipType = "How to write conclusion"
        } else if sender.tag == 3 {
            tipType = "Discussion Essay"
        } else if sender.tag == 4 {
            tipType = "Opinion Esaay"
        } else if sender.tag == 5 {
            tipType = "Discussion/Opinion Essay"
        } else if sender.tag == 6 {
            tipType = "Situation Essay"
        }
        
        if sender.tag != 0 {
        #if Free
        guard let url = URL(string: "https://itunes.apple.com/us/app/ielts-ideas/id1438824193") else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        #else
        performSegue(withIdentifier: "tipsDetails", sender: self)
        #endif
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "tipsDetails" {
            let dvc = segue.destination as! TipsDetailViewController
            dvc.essayTip = tipType
        }
    }
    

}
