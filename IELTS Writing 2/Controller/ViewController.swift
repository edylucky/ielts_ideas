//
//  ViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 1/23/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
//import CoreData
import Firebase
import SafariServices
import GoogleMobileAds

class ViewController: UIViewController {
    
    var db: Firestore!
    
    @IBOutlet weak var buttonIdeas: UIButton!
    @IBOutlet weak var buttonTips: UIButton!
    @IBOutlet weak var buttonRecent: UIButton!
    @IBOutlet weak var buttonAssessment: UIButton!
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var ideasLabel: UILabel!
    
    var widthMultiplier = 0.0
    var heightMultiplier = 0.0
    
    var interstitial: GADInterstitial?
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.tabBarController?.tabBar.isHidden = true
       // self.navigationController?.isNavigationBarHidden = true
        
        
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
      //  addEssay()
        
        
        
        
//        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
//
//        let newEssay = Ideas(context: context)
//        newEssay.essayName = "salam1"
//        do {
//            try context.save()
//        } catch {
//            print("error saving \(error)")
//        }
//
        
        
        buttonIdeas.layer.borderWidth = 1
        buttonTips.layer.borderWidth = 1
        buttonRecent.layer.borderWidth = 1
        buttonAssessment.layer.borderWidth = 1
        buttonRegister.layer.borderWidth = 1
        
        // Do any additional setup after loading the view, typically from a nib.
        
        
        widthMultiplier = Double(self.view.frame.size.width) /  122
        heightMultiplier = Double(self.view.frame.size.height) / 109
        
        buttonRecent.frame.size.width = buttonRecent.frame.width * CGFloat(widthMultiplier)
        buttonAssessment.frame.size.height = buttonAssessment.frame.height * CGFloat(heightMultiplier)
        
        
        ideasLabel.numberOfLines = 1
        ideasLabel.adjustsFontSizeToFitWidth = true
        
        #if Free
        interstitial = createAndLoadInterstitial()
        #endif

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        

    }
    
    @IBAction func ideasButtonPressed(_ sender: Any) {
       // performSegue(withIdentifier: "showIdeas", sender: self)
       // performSegue(withIdentifier: "showTabBar", sender: self)
        
        tabBarController?.selectedIndex = 1
        self.tabBarController?.tabBar.isHidden = false
        
        #if Free
        interstitial = createAndLoadInterstitial()
        #endif
    }
    
    @IBAction func assessButtonPressed(_ sender: Any) {
        
        tabBarController?.selectedIndex = 4
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    @IBAction func recentButtonPressed(_ sender: Any) {
        
        tabBarController?.selectedIndex = 3
        self.tabBarController?.tabBar.isHidden = false
        
        #if Free
        interstitial = createAndLoadInterstitial()
        #endif
        
    }
    
    
    
    
    private func addEssay() {
        // [START add_ada_lovelace]
        // Add a new document with a generated ID
        var ref: DocumentReference? = nil
        ref = db.collection("essays").addDocument(data: [
            "topic": "Art",
            "type": "S",
            "name": "",
            "P1": "",
            "P1s1": "",
            "P1s2": "",
            "P2": "",
            "P2s1": "",
            "P2s2": ""
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
        // [END add_ada_lovelace]
    }
    

    
    @IBAction func tipsButtonPressed(_ sender: Any) {
        
        tabBarController?.selectedIndex = 2
        self.tabBarController?.tabBar.isHidden = false
        
        #if Free
        interstitial = createAndLoadInterstitial()
        #endif
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "showIdeas" {
            let dvc = segue.destination as! TopicsTableViewController
            dvc.self.navigationItem.leftBarButtonItem = nil
          
            
        }
        
        
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        
        
        if let url = URL(string: "https://www.ielts.org/book-a-test/how-do-i-register") {
            let safariController = SFSafariViewController(url: url)
            present(safariController, animated: true, completion: nil)
        }
        
        
    }
    
    
    private func createAndLoadInterstitial() -> GADInterstitial? {
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-6949238361800871/5132644393")
        
        guard let interstitial = interstitial else {
            return nil
        }
        
        let request = GADRequest()
        // Remove the following line before you upload the app
        // request.testDevices = [ kGADSimulatorID ]
        interstitial.load(request)
        interstitial.delegate = self
        
        return interstitial
    }
    
    

}
extension UIViewController: GADInterstitialDelegate {
    public func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("Interstitial loaded successfully")
        ad.present(fromRootViewController: self)
    }
    
    public func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        print("Fail to receive interstitial")
    }
}

