//
//  TopicsTableViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 1/25/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit

class TopicsTableViewController: UITableViewController {
    
    var topics = ["Art", "Business & Money" , "Communication", "Crime & Punishment" , "Education" , "Environment" , "Family & Children", "Food & Diet", "Government and Politics", "Health", "Housing and Buildings", "Language", "Leisure & Free Time", "Media & Advertising","Reading",  "Society", "Science & Space",  "Sport & Exercise", "Technology",  "Transport", "Travel & Tourism",   "Work & Employment"]
    var topicImages = ["art.png","money.png", "communication.png", "crime.png", "education.png" , "environment.png" , "family.pnd", "food.png", "government.png", "health.png", "house.png", "language.png", "leisure.png", "media.pnd","reading.png", "society.png", "science.png",  "sport.png", "technology.png",  "transport.png", "travel.png",   "work.png" ]
    
    var barTitle = ""
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.tabBarController?.selectedIndex = 1
        
       // print("secilmistab \(self.tabBarController?.selectedIndex)")
        
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "left-angle-bracket.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(TopicsTableViewController.back(sender:)))
       // let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TopicsTableViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
 
        
        
        self.tableView.rowHeight = 80

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    


    @objc func back(sender: UIBarButtonItem) {
    
        //let vc = self.storyboard?.instantiateViewController(withIdentifier: "mainView") as! ViewController
       // self.navigationController?.pushViewController(vc, animated: true)
        
        tabBarController?.selectedIndex = 0

}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return topics.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                 for: indexPath) as! TopicTableViewCell
        // Configure the cell...
        cell.nameLabel.text = topics[indexPath.row]
        cell.thumbnailImageView.image = UIImage(named: topicImages[indexPath.row])
        
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            if let indexPath = tableView.indexPathForSelectedRow {
                let essayTypesVC = segue.destination as! EssayTypesViewController
                essayTypesVC.essayTopic = topics[indexPath.row]
                    
            }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //self.tabBarController?.tabBar.isHidden = true
        
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
