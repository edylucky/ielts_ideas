//
//  AppDelegate.swift
//  IELTS Writing 2
//
//  Created by Edy on 1/23/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //print(Realm.Configuration.defaultConfiguration.fileURL)
        
        /*let data = Data()
        data.essayName = "Test"
        data.essayPlan = "testestst"
        data.essayTopic = "Art"
        data.essayType = "D"
 */
        
     /*   do {
        let realm = try Realm()
            try realm.write {
                realm.add(data)
            }
        }
        catch {
            print("error initialising \(error)")
        }
        */
        
        FirebaseApp.configure()
        
        let db = Firestore.firestore()
        
        #if Free
        GADMobileAds.configure(withApplicationID: "ca-app-pub-6949238361800871~7602895063")
        #endif
        
        return true
    }

   

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
      
        let container = NSPersistentContainer(name: "DataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
               
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


}

