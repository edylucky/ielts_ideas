//
//  EssayTypesViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 1/27/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import SafariServices

class EssayTypesViewController: UIViewController {
    
    
    var db: Firestore!
    

    
    var ideasArray = [Ideas]()
    var essayArray = [String]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var essayType = ""
    var essayTopic = ""
  
    @IBOutlet weak var navTitle: UINavigationItem!
    @IBOutlet weak var generalButton: UIButton!
    @IBOutlet weak var discussionButton: UIButton!
    @IBOutlet weak var opinionButton: UIButton!
    @IBOutlet weak var opdiscButton: UIButton!
    @IBOutlet weak var situationButton: UIButton!
    @IBOutlet weak var vocabularyButton: UIButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(essayTopic)
        
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()

        // Do any additional setup after loading the view.
        
        //fetchEssays()
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
       // self.navigationController?.title = essayTopic
        navTitle.title = essayTopic
        generalButton.layer.borderWidth = 1
        discussionButton.layer.borderWidth = 1
        opinionButton.layer.borderWidth = 1
        opdiscButton.layer.borderWidth = 1
        situationButton.layer.borderWidth = 1
        vocabularyButton.layer.borderWidth = 1
        
//        if essayTopic != "Art" {
//            
//            generalButton.isEnabled = false
//        }
//        
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    
    @IBAction func essayTypeButtonPressed(_ sender: AnyObject) {
        
        if essayTopic != "Art" {
            
        
        
        #if Free

        guard let url = URL(string: "https://itunes.apple.com/us/app/ielts-ideas/id1438824193") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        #else
        
        
            if sender.tag == 0 {
                
                performSegue(withIdentifier: "generalIdeas", sender: self)
                
            } else if sender.tag == 1 {
                essayType = "D"
                performSegue(withIdentifier: "essayPlans", sender: self)
                
            } else if sender.tag == 2 {
                print("opinion essay")
                essayType = "O"
                performSegue(withIdentifier: "essayPlans", sender: self)
            } else if sender.tag == 3 {
                print("discussion/opinion")
                essayType = "OD"
                performSegue(withIdentifier: "essayPlans", sender: self)
            } else if sender.tag == 4 {
                print("situation")
                essayType = "S"
                performSegue(withIdentifier: "essayPlans", sender: self)
            } else if sender.tag == 5 {
                performSegue(withIdentifier: "topicVocabulary", sender: self)
            }
        
        #endif
        
        } else if essayTopic == "Art" {
            
            
            if sender.tag == 0 {

                performSegue(withIdentifier: "generalIdeas", sender: self)
                
            } else if sender.tag == 1 {
                essayType = "D"
                performSegue(withIdentifier: "essayPlans", sender: self)
                
            } else if sender.tag == 2 {
                print("opinion essay")
                essayType = "O"
                performSegue(withIdentifier: "essayPlans", sender: self)
            } else if sender.tag == 3 {
                print("discussion/opinion")
                essayType = "OD"
                performSegue(withIdentifier: "essayPlans", sender: self)
            } else if sender.tag == 4 {
                print("situation")
                essayType = "S"
                performSegue(withIdentifier: "essayPlans", sender: self)
            } else if sender.tag == 5 {
                performSegue(withIdentifier: "topicVocabulary", sender: self)
            }
        }
    }
    
    
    func fetchEssays() {
        
        let request : NSFetchRequest<Ideas> = Ideas.fetchRequest()
        

        do {
            ideasArray = try context.fetch(request)
        }  catch {
            print("Error fetching data from context \(error)")
        }
    }
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        
        if segue.identifier == "essayPlans" {
         let dvc = segue.destination as! EssayText1ViewController
        //dvc.ideasArray1 = ideasArray
        dvc.essayType = essayType
        dvc.essayTopic = essayTopic
       
        } else if segue.identifier == "generalIdeas" {
            
          
            let dvc = segue.destination as! GeneralIdeasViewController
            dvc.essayTopic = essayTopic
   
            
        } else if segue.identifier == "topicVocabulary" {
            
            let dvc = segue.destination as! VocabularyViewController
            dvc.essayTopic = essayTopic
        }
        
        
        
        
    }
    
    
  
    
    
    private func getCollection() {
        // [START get_collection]
        db.collection("essays").whereField("type", isEqualTo: "D").getDocuments() { (snapshot, error) in
            if  error != nil {
                print("Error getting documents: \(String(describing: error))")
            } else {
                for document in snapshot!.documents {
                    
                    if let name = document.data()["name"] as? String {
                         //print(name)
                        
                        self.essayArray.append(name)
                     
 
                        }
                    
                    }
                print(self.essayArray[0])
                    
                }
            }
        }
        // [END get_collection]
        
        
        
        
        
    }
    
    

