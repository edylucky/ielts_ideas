//
//  EssayTextViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 1/27/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class EssayTextViewController: UIViewController {
    
    
    var db: Firestore!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var planLabel: UILabel!
    
    @IBOutlet weak var p1Label: UILabel!
    @IBOutlet weak var P1S1Label: UILabel!
    @IBOutlet weak var P1S2Label: UILabel!
    @IBOutlet weak var P1S3Label: UILabel!
    @IBOutlet weak var P1S4Label: UILabel!
    @IBOutlet weak var P2Label: UILabel!
    @IBOutlet weak var P2S1Label: UILabel!
    @IBOutlet weak var P2S2Label: UILabel!
    @IBOutlet weak var P2S3Label: UILabel!
    @IBOutlet weak var P2S4Label: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var backButton: UIButton!
    
    var ideasArray = [Ideas]()
    var ideasArray1 = [Ideas]()
    var essayArray = [String]()
    var essayArrayP1 = [String]()
    var essayArrayP1s1 = [String]()
    var essayArrayP1s2 = [String]()
    var essayArrayP1s3 = [String]()
    var essayArrayP1s4 = [String]()
    var essayArrayP2 = [String]()
    var essayArrayP2s1 = [String]()
    var essayArrayP2s2 = [String]()
    var essayArrayP2s3 = [String]()
    var essayArrayP2s4 = [String]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var questionNumber = 0
    
    var essayType = ""
    var essayTopic = ""
    
    var paraghSen = ""
    
    
    


   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // call firestore
        
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
        //getCollection(essayType: essayType)
        
     //   fetchEssays()
        
        // updateLables()
        
        if essayType == "D" {
            self.title = "Discussion"
        } else if essayType == "O" {
            self.title = "Opinion"
        } else if essayType == "OD" {
            self.title = "Discussion/Opinion"
        } else if essayType == "S" {
            self.title = "Situation"
        }
        
        
        print(ideasArray.count)
        print(essayType)
        
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
       // backButton.isEnabled = false
        
        
        nameLabel.text = "Please wait...."
        p1Label.text = "salair work. To what extent do you agree?3.salam  "
       /* P1S1Label.text = ""
        P1S2Label.text = ""
        P1S3Label.text = ""
        P1S4Label.text = "" */
        P2Label.text = "salam"
//        P2S1Label.text = ""
//        P2S2Label.text = ""
//        P2S3Label.text = ""
//        P2S4Label.text = ""
        
       
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    func fetchEssays() {
        let request : NSFetchRequest<Ideas> = Ideas.fetchRequest()
        do {
            ideasArray = try context.fetch(request)
        }  catch {
            print("Error fetching data from context \(error)")
        }
    }
    
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
       // backButton.isEnabled = true
        
       
        
        
        questionNumber = questionNumber + 1
        
        if questionNumber < essayArray.count {
            nameLabel.text = essayArray[questionNumber]
            p1Label.text = essayArrayP1[questionNumber]
            P1S1Label.text = essayArrayP1s1[questionNumber]
            P1S2Label.text = essayArrayP1s2[questionNumber]
            P1S3Label.text = essayArrayP1s3[questionNumber]
            P1S4Label.text = essayArrayP1s4[questionNumber]
            P2Label.text = essayArrayP2[questionNumber]
            P2S1Label.text = essayArrayP2s1[questionNumber]
            P2S2Label.text = essayArrayP2s2[questionNumber]
            P2S3Label.text = essayArrayP2s3[questionNumber]
            P2S4Label.text = essayArrayP2s4[questionNumber]
            print(essayArray[questionNumber])
            print(questionNumber)
            
            underLine(p_label: p1Label)
            underLine(p_label: P2Label)
            
            
        } else {
            let alert = UIAlertController(title: "Awesome", message: "You've finished all the questions for this section", preferredStyle: .alert)
            
            let restartAction = UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                self.startOver()
            })
            
            alert.addAction(restartAction)
            
            present(alert, animated: true, completion: nil)
        }
        

        
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        
        
        if questionNumber == 0 {
            
            backButton.isEnabled = false
        }
        questionNumber = questionNumber - 1
        
        if questionNumber > 0 {
            nameLabel.text = essayArray[questionNumber]
            p1Label.text = essayArrayP1[questionNumber]
            P1S1Label.text = essayArrayP1s1[questionNumber]
            P1S2Label.text = essayArrayP1s2[questionNumber]
            P1S3Label.text = essayArrayP1s3[questionNumber]
            P1S4Label.text = essayArrayP1s4[questionNumber]
            P2Label.text = essayArrayP2[questionNumber]
            P2S1Label.text = essayArrayP2s1[questionNumber]
            P2S2Label.text = essayArrayP2s2[questionNumber]
            P2S3Label.text = essayArrayP2s3[questionNumber]
            P2S4Label.text = essayArrayP2s4[questionNumber]
            print(essayArray[questionNumber])
            print(questionNumber)
            
            underLine(p_label: p1Label)
            underLine(p_label: P2Label)
            
            
        } else {
            let alert = UIAlertController(title: "Oppss", message: "This is the first question", preferredStyle: .alert)
            
            let restartAction = UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                self.startOver()
            })
            
            alert.addAction(restartAction)
            
            present(alert, animated: true, completion: nil)
        }
        
        
        
    }
    
    
    func startOver() {
        
        questionNumber = 0
        
        updateLables()
        
    }
    
    func updateLables() {
        
        //if ideasArray[1].essayType == essayType {
       // nameLabel.text = ideasArray[1].essayName
        //}
        
        nameLabel.text = essayArray[0]
        p1Label.text = essayArrayP1[0]
        //P1S1Label.text = essayArrayP1s1[0]
        
//        paraghSen = essayArrayP1s1[0]
//        P1S1Label.text  = paraghSen.replacingOccurrences(of: "_b", with: "\n  ")
//
//
//        P1S2Label.text = essayArrayP1s2[0]
//        P1S3Label.text = essayArrayP1s3[0]
//        P1S4Label.text = essayArrayP1s4[0]
        P2Label.text = essayArrayP2[0]
//        P2S1Label.text = essayArrayP2s1[0]
//        P2S2Label.text = essayArrayP2s2[0]
//        P2S3Label.text = essayArrayP2s3[0]
//        P2S4Label.text = essayArrayP2s4[0]
        
        underLine(p_label: p1Label)
        underLine(p_label: P2Label)

        
    }
    
    func underLine (p_label: UILabel) {
        
        let underlineAttribute = [kCTUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: p_label.text! , attributes: underlineAttribute as [NSAttributedStringKey : Any])
        p_label.attributedText = underlineAttributedString
       /* let underlineAttributedString1 = NSAttributedString(string: P2Label.text! , attributes: underlineAttribute as [NSAttributedStringKey : Any])
        P2Label.attributedText = underlineAttributedString1
 */
        
    }
    
    
    private func getCollection(essayType: String) {
        // [START get_collection]
       //activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
      //  activityIndicator.startAnimating()
        
        db.collection("essays").whereField("type", isEqualTo: essayType).getDocuments() { (snapshot, error) in
            if  error != nil {
                print("Error getting documents: \(error)")
            } else {
                for document in snapshot!.documents {
                    
                    if let name = document.data()["name"] as? String, let P1 = document.data()["P1"] as? String, let P1s1 = document.data()["P1s1"] as? String, let P1s2 = document.data()["P1s2"] as? String, let P1s3 = document.data()["P1s3"] as? String, let P1s4 = document.data()["P1s4"] as? String, let P2 = document.data()["P2"] as? String, let P2s1 = document.data()["P2s1"] as? String, let P2s2 = document.data()["P2s2"] as? String, let P2s3 = document.data()["P2s3"] as? String, let P2s4 = document.data()["P2s4"] as? String
                        {
                        //print(name)
                        
                        self.essayArray.append(name)
                        self.essayArrayP1.append(P1)
                        self.essayArrayP1s1.append(P1s1)
                        self.essayArrayP1s2.append(P1s2)
                        self.essayArrayP1s3.append(P1s3)
                        self.essayArrayP1s4.append(P1s4)
                        self.essayArrayP2.append(P2)
                        self.essayArrayP2s1.append(P2s1)
                        self.essayArrayP2s2.append(P2s2)
                        self.essayArrayP2s3.append(P2s3)
                        self.essayArrayP2s4.append(P2s4)
                    }
                    
                }
                print(self.essayArray[0])
               self.updateLables()
              // self.activityIndicator.stopAnimating()
              //  self.activityIndicator.hidesWhenStopped = true
            }
        }
    }
    
    
    
    
    
}
