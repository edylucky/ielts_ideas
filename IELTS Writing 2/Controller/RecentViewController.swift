//
//  RecentViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/13/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import Firebase

class RecentViewController: UIViewController , UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var db: Firestore!
    
    let list = ["Somehdhdhdhdhdhdhdhdhdhdhdhdh people think that art is an essential subject for children at school while others think it is a waste of time. Discuss both sides and give your opinion.","Luckysalamamamamammmmsmmdmdmdmdmdmdmdmdmmdmd","Parkt"]
    var essayName = [String]()
    var essayMonth = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.estimatedRowHeight = 140
        
      
        
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
      //  addEssay()
        getCollection()
        
  
        
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.selectedIndex = 3
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "left-angle-bracket.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(TopicsTableViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        tableView.isHidden = true

    }
    
    @objc func back(sender: UIBarButtonItem) {
        tabBarController?.selectedIndex = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return essayName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       // let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "myCell")
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! RecentTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecentTableViewCell
        
        let essName = essayName[indexPath.row]
        
        
        cell.nameLabel.text = essName
       // cell.numberLabel.text = String(indexPath.row)
        //cell.textLabel?.text = essName
        //cell.nameLabel.text = essName
        
      //  cell.textLabel?.text = essayName[indexPath.row]
        
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
      
    }
    
    
    

    private func addEssay() {
     
        var ref: DocumentReference? = nil
        ref = db.collection("recentEssays").addDocument(data: [
            "Month": "September - 2018",
            "name": "Some people think that art is an essential subject for children at school while others think it is a waste of time. Discuss both sides and give your opinion."
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
        
    }
    
    
    private func getCollection() {
        // [START get_collection]
        activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        activityIndicator.startAnimating()
        
        db.collection("recentEssays").getDocuments() { (snapshot, error) in
            if  error != nil {
                print("Error getting documents: \(error)")
            } else {
                for document in snapshot!.documents {
                    
                    if let name = document.data()["name"] as? String, let month = document.data()["Month"] as? String
                    {
                        //print(name)
                        
                        self.essayName.append(name)
                        self.essayMonth.append(month)
                       
                    }
                    
                }
                print(self.essayName[0])
                
                self.tableView.reloadData()
              //  self.updateLables()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidesWhenStopped = true
                self.tableView.isHidden = false
            }
        }
    }

}
