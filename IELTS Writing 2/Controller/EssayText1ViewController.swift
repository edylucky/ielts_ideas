//
//  EssayText1ViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/18/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import Firebase

class EssayText1ViewController: UIViewController {
    
    var db: Firestore!
    
    @IBOutlet weak var genIdeaLabel: UILabel!
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var P1Label: UILabel!
    @IBOutlet weak var P1S1Label: UILabel!
    @IBOutlet weak var P1S2Label: UILabel!
    @IBOutlet weak var P2Label: UILabel!
    @IBOutlet weak var P2S1Label: UILabel!
    @IBOutlet weak var P2S2Label: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var horSlider: UISlider!
    
    var essayArray = [String]()
    var essayArrayP1 = [String]()
    var essayArrayP1S1 = [String]()
    var essayArrayP1S2 = [String]()
    var essayArrayP2 = [String]()
    var essayArrayP2S1 = [String]()
    var essayArrayP2S2 = [String]()
    
    var questionNumber = 0
    var essayType = ""
    var essayTopic = ""
    var paraghSen = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // call firestore
        
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()

        genIdeaLabel.text = "Please wait.."
        planLabel.text = ""
        
        P1Label.text = ""
        P1S1Label.text = ""
        P1S2Label.text = ""
        P2Label.text = ""
        P2S1Label.text = ""
        P2S2Label.text = ""
        
        
        
        if essayType == "D" {
            self.title = "Discussion"
        } else if essayType == "O" {
            self.title = "Opinion"
        } else if essayType == "OD" {
            self.title = "Discussion/Opinion"
        } else if essayType == "S" {
            self.title = "Situation"
        }
        
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
        getCollection(essayType: essayType)
        
        backButton.isEnabled = false
        
        horSlider.isHidden = true
        sliderView.isHidden = true
        
        self.hideKeyboardWhenTappedAround()
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func updateLables(p_question_num : Int) {
        
        if essayArray.count == 1 {
            nextButton.isEnabled = false
        }
        
        genIdeaLabel.text = essayArray[p_question_num]
        P1Label.text = essayArrayP1[p_question_num]
        paraghSen = essayArrayP1S1[p_question_num]
        paraghSen = paraghSen.replacingOccurrences(of: "_b2", with: "\n\u{2022} ") //enter+bullet_point
        paraghSen = paraghSen.replacingOccurrences(of: "_b1", with: "\u{2022} ") // first_bullet_point
        paraghSen = paraghSen.replacingOccurrences(of: "_b", with: "\n  ") // enter
        P1S1Label.text = paraghSen
        P1S2Label.text = essayArrayP1S2[p_question_num]
        
        
        P2Label.text = essayArrayP2[p_question_num]
        paraghSen = essayArrayP2S1[p_question_num]
        paraghSen = paraghSen.replacingOccurrences(of: "_b2", with: "\n\u{2022} ") //enter+bullet_point
        paraghSen = paraghSen.replacingOccurrences(of: "_b1", with: "\u{2022} ") // first_bullet_point
        paraghSen = paraghSen.replacingOccurrences(of: "_b", with: "\n  ") // enter
        P2S1Label.text = paraghSen
        P2S2Label.text = essayArrayP2S2[p_question_num]
        

        underLine(p_label: P1Label)
        underLine(p_label: P2Label)
        
        planLabel.text = "Plan"
        
        
    }
    
    func underLine (p_label: UILabel) {
        
        let underlineAttribute = [kCTUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: p_label.text! , attributes: underlineAttribute as [NSAttributedStringKey : Any])
        p_label.attributedText = underlineAttributedString
        
        
    }
    
    private func getCollection(essayType: String) {
        // [START get_collection]
        activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        activityIndicator.startAnimating()
        
        db.collection("essays").whereField("type", isEqualTo: essayType).whereField("topic", isEqualTo: essayTopic).getDocuments() { (snapshot, error) in
            if  error != nil {
                print("Error getting documents: \(String(describing: error))")
            } else {
                for document in snapshot!.documents {
                    
                    if let name = document.data()["name"] as? String, let P1 = document.data()["P1"] as? String, let P1S1 = document.data()["P1s1"] as? String, let P1S2 = document.data()["P1s2"] as? String,  let P2 = document.data()["P2"] as? String, let P2S1 = document.data()["P2s1"] as? String, let P2S2 = document.data()["P2s2"] as? String
                    {
                        print(name)
                        
                        self.essayArray.append(name)
                        self.essayArrayP1.append(P1)
                        self.essayArrayP1S1.append(P1S1)
                        self.essayArrayP1S2.append(P1S2)
                        self.essayArrayP2.append(P2)
                        self.essayArrayP2S1.append(P2S1)
                        self.essayArrayP2S2.append(P2S2)
                    
                    }
                    
                }
                if self.essayArray.count > 0 {
                print(self.essayArray[0])
                self.updateLables(p_question_num: 0)
                } else {
                    self.genIdeaLabel.text = "Sorry, for this section we do not have any question."
                }
                 self.activityIndicator.stopAnimating()
                 self.activityIndicator.hidesWhenStopped = true
                    
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        questionNumber = questionNumber - 1
        planLabel.text = "Plan"
        if questionNumber <= 0 {
            
            backButton.isEnabled = false
            updateLables(p_question_num: 0)
        } else {
            updateLables(p_question_num: questionNumber)
        }
        
        
        
        
        
//        if questionNumber > 0 {
//
//            updateLables(p_question_num: questionNumber)
//
//
//        }
//        else {
//            let alert = UIAlertController(title: "Oppss", message: "This is the first question", preferredStyle: .alert)
//
//            let restartAction = UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
//                self.startOver()
//            })
//
//            alert.addAction(restartAction)
//
//            present(alert, animated: true, completion: nil)
//        }
        
        
    }
    
    func startOver() {
        
        questionNumber = 0
        
        updateLables(p_question_num: questionNumber)
        
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
         backButton.isEnabled = true
        
        planLabel.text = "Plan"
        
        
        questionNumber = questionNumber + 1
        
        if questionNumber < essayArray.count {
            
            updateLables(p_question_num: questionNumber)
            
        } else {
            let alert = UIAlertController(title: "Information", message: "You've finished all the questions for this section", preferredStyle: .alert)
            
            let restartAction = UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                self.startOver()
            })
            
            alert.addAction(restartAction)
            
            present(alert, animated: true, completion: nil)
        }
        
        
        
    }
    
    
    // MARK: - aAButton
    
    
    @IBAction func aAButtonPressed(_ sender: Any) {
        horSlider.isHidden = false
        sliderView.isHidden = false
    }
    
    
    @IBAction func slidderButtonChanged(_ sender: UISlider) {
        
        
        print(Int(sender.value))
        genIdeaLabel.font = genIdeaLabel.font.withSize(CGFloat(Int(sender.value)))
        planLabel.font = planLabel.font.withSize(CGFloat(Int(sender.value)))
        P1Label.font = P1Label.font.withSize(CGFloat(Int(sender.value)))
        P1S1Label.font = P1S1Label.font.withSize(CGFloat(Int(sender.value)))
        P1S2Label.font = P1S2Label.font.withSize(CGFloat(Int(sender.value)))
        P2Label.font = P2Label.font.withSize(CGFloat(Int(sender.value)))
        P2S1Label.font = P2S1Label.font.withSize(CGFloat(Int(sender.value)))
        P2S2Label.font = P2S2Label.font.withSize(CGFloat(Int(sender.value)))
      
    }
    
   
    
}
extension EssayText1ViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EssayText1ViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        horSlider.isHidden = true
        sliderView.isHidden = true
    }
}
