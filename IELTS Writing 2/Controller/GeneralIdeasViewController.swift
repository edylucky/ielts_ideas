//
//  GeneralIdeasViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/17/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import Firebase

class GeneralIdeasViewController: UIViewController {
    
    var db: Firestore!
    var topic = [String]()
    var topicSentence = ""
    var essayTopic = ""
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var genIdeaLabel: UILabel!
    @IBOutlet weak var horSlider: UISlider!
    @IBOutlet weak var sliderView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
        genIdeaLabel.text = "Please wait...."
        
        //addIdea()

        getCollection(topic : essayTopic)

        horSlider.isHidden = true
        sliderView.isHidden = true
        
        self.hideSliderWhenTappedAround()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addIdea() {
        
        var ref: DocumentReference? = nil
        ref = db.collection("generalIdeas").addDocument(data: [
            "topic": "Art",
            "idea": "Some people think that art is an essential subject for children at school while others think it is a waste of time. Discuss both sides and give your opinion."
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
        
    }
    
    
    private func getCollection(topic : String) {
        // [START get_collection]
        activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
         activityIndicator.startAnimating()
        
        db.collection("generalIdeas").whereField("topic", isEqualTo: topic).getDocuments() { (snapshot, error) in
            if  error != nil {
                print("Error getting documents: \(String(describing: error))")
            } else {
                for document in snapshot!.documents {
                    
                    if let idea = document.data()["idea"] as? String
                    {
                        //print(name)
                        
                        self.topic.append(idea)
                      
                        
                    }
                    
                }
                
               // self.genIdeaLabel.text = self.topic[0]
                
               // self.topicSentence = "\u{2022} \(self.topic[0])"
               // self.genIdeaLabel.text  = self.topicSentence.replacingOccurrences(of: "_b", with: "\n\u{2022} ")
                 // self.updateLables()
                var paraghSen = self.topic[0]
                paraghSen = paraghSen.replacingOccurrences(of: "_b2", with: "\n\u{2022} ") //enter+bullet_point
                paraghSen = paraghSen.replacingOccurrences(of: "_b1", with: "\u{2022} ") // first_bullet_point
                paraghSen = paraghSen.replacingOccurrences(of: "_b", with: "\n  ") // enter
                
                self.genIdeaLabel.text = paraghSen
                  self.activityIndicator.stopAnimating()
                self.activityIndicator.hidesWhenStopped = true
            }
        }
    }
    
  // MARK: - aAButton for general ideas
    
    
    @IBAction func aAButtonPressed(_ sender: Any) {
        
        sliderView.isHidden = false
        horSlider.isHidden = false
    }
    
    @IBAction func horSliderChanged(_ sender: UISlider) {
        
        
        genIdeaLabel.font = genIdeaLabel.font.withSize(CGFloat(Int(sender.value)))
        
    }
    
    
    

}
extension GeneralIdeasViewController {
    func hideSliderWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GeneralIdeasViewController.dismissSlider))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissSlider() {
        view.endEditing(true)
        horSlider.isHidden = true
        sliderView.isHidden = true
    }
}
