//
//  TipsDetailViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/29/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import Firebase

class TipsDetailViewController: UIViewController {
    
    
    var db: Firestore!
    var tipsArray = [Tips]()
    var essayTip = ""
    
    
    @IBOutlet weak var tipNameLabel: UILabel!
    @IBOutlet weak var Detail1: UILabel!
    @IBOutlet weak var Detail2: UILabel!
    @IBOutlet weak var Detail3: UILabel!
    @IBOutlet weak var Detail4: UILabel!
    @IBOutlet weak var Detail5: UILabel!
    @IBOutlet weak var Detail6: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var Detail7: UILabel!
    @IBOutlet weak var Detail8: UILabel!
    @IBOutlet weak var Detail9: UILabel!
    @IBOutlet weak var Detail10: UILabel!
    @IBOutlet weak var Detail11: UILabel!
    @IBOutlet weak var Detail12: UILabel!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var horSlider: UISlider!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
       // addVocab()
       getCollection()
        
        sliderView.isHidden = true
        horSlider.isHidden = true
        
        self.hideSliderWhenTappedAround()
        
        Detail1.text = "Please wait.."
    }
    
    private func addVocab() {
        
        var ref: DocumentReference? = nil
        ref = db.collection("essayTips").addDocument(data: [
            "tipName" : "",
            "detail1" : "",
            "detail2" : "",
            "detail3" : "",
            "detail4" : "",
            "detail5" : "",
            "detail6" : "",
            "detail7" : "",
            "detail8" : "",
            "detail9" : "",
            "detail10" : "",
            "detail11" : "",
            "detail12" : ""
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
        
    private func getCollection() {
        // [START get_collection]
        
        activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        activityIndicator.startAnimating()
        
        db.collection("essayTips").whereField("tipName", isEqualTo: essayTip).getDocuments() { (snapshot, error) in
            if  error != nil {
                print("Error getting documents: \(String(describing: error))")
            } else {
                for document in snapshot!.documents {
                    
                    if let detail1 = document.data()["detail1"] as? String, let detail2 = document.data()["detail2"] as? String, let detail3 = document.data()["detail3"] as? String, let detail4 = document.data()["detail4"] as? String, let detail5 = document.data()["detail5"] as? String, let detail6 = document.data()["detail6"] as? String, let detail7 = document.data()["detail7"] as? String, let detail8 = document.data()["detail8"] as? String, let detail9 = document.data()["detail9"] as? String, let detail10 = document.data()["detail10"] as? String, let detail11 = document.data()["detail11"] as? String, let detail12 = document.data()["detail12"] as? String
                    {
                        print(self.essayTip)

                        let item = Tips(p_tipName: self.essayTip, p_detail1: detail1, p_detail2: detail2, p_detail3: detail3, p_detail4: detail4, p_detail5: detail5, p_detail6: detail6, p_detail7: detail7, p_detail8: detail8, p_detail9: detail9, p_detail10: detail10, p_detail11: detail11, p_detail12: detail12)
                        self.tipsArray.append(item)
                        
                    }
                    
                }
                
               self.updateLables(p_question_num: 0)
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidesWhenStopped = true
                
            }
        }
    }

    
    func updateLables(p_question_num : Int) {
        
        
         tipNameLabel.text = essayTip
        Detail1.text = replaceText(p_text: tipsArray[p_question_num].detail1)
        Detail2.text = replaceText(p_text: tipsArray[p_question_num].detail2)
        Detail3.text = replaceText(p_text: tipsArray[p_question_num].detail3)
        Detail4.text = replaceText(p_text: tipsArray[p_question_num].detail4)
        Detail5.text = replaceText(p_text: tipsArray[p_question_num].detail5)
        Detail6.text = replaceText(p_text: tipsArray[p_question_num].detail6)
        Detail7.text = replaceText(p_text: tipsArray[p_question_num].detail7)
        Detail8.text = replaceText(p_text: tipsArray[p_question_num].detail8)
        Detail9.text = replaceText(p_text: tipsArray[p_question_num].detail9)
        Detail10.text = replaceText(p_text: tipsArray[p_question_num].detail10)
        Detail11.text = replaceText(p_text: tipsArray[p_question_num].detail11)
        Detail12.text = replaceText(p_text: tipsArray[p_question_num].detail12)
        
        
    }
    
    
    func replaceText (p_text :String) -> String {
        
        var paraghSen = p_text
        paraghSen = paraghSen.replacingOccurrences(of: "_b2", with: "\n\u{2022} ") //enter+bullet_point
        paraghSen = paraghSen.replacingOccurrences(of: "_b1", with: "\u{2022} ") // first_bullet_point
        paraghSen = paraghSen.replacingOccurrences(of: "_b", with: "\n  ") // enter
        return paraghSen
    }
    
    
    // MARK: - aAButton
    
    
    @IBAction func aAButtonPressed(_ sender: Any) {
        sliderView.isHidden = false
        horSlider.isHidden = false
    }
    
    
    // MARK: - sliderButton

    @IBAction func sliderButtonChanged(_ sender: UISlider) {
        
        
        
        tipNameLabel.font = tipNameLabel.font.withSize(CGFloat(Int(sender.value)))
        Detail1.font = Detail1.font.withSize(CGFloat(Int(sender.value)))
        Detail2.font = Detail2.font.withSize(CGFloat(Int(sender.value)))
        Detail3.font = Detail3.font.withSize(CGFloat(Int(sender.value)))
        Detail4.font = Detail4.font.withSize(CGFloat(Int(sender.value)))
        Detail5.font = Detail5.font.withSize(CGFloat(Int(sender.value)))
        Detail6.font = Detail6.font.withSize(CGFloat(Int(sender.value)))
        Detail7.font = Detail7.font.withSize(CGFloat(Int(sender.value)))
        Detail8.font = Detail8.font.withSize(CGFloat(Int(sender.value)))
        Detail9.font = Detail9.font.withSize(CGFloat(Int(sender.value)))
        Detail10.font = Detail10.font.withSize(CGFloat(Int(sender.value)))
        Detail11.font = Detail11.font.withSize(CGFloat(Int(sender.value)))
        Detail12.font = Detail12.font.withSize(CGFloat(Int(sender.value)))

        
        
    }
}
extension TipsDetailViewController {
    func hideSliderWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TipsDetailViewController.dismissSlider))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissSlider() {
        view.endEditing(true)
        horSlider.isHidden = true
        sliderView.isHidden = true
    }
}
