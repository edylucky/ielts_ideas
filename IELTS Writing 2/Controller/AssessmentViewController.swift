//
//  AssessmentViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/8/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import MessageUI

class AssessmentViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var messageField: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var contactLabel: UILabel!
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.selectedIndex = 4
        sendButton.layer.borderWidth = 1
        nameField.layer.borderWidth = 1
        emailField.layer.borderWidth = 1
        messageField.layer.borderWidth = 1
       // self.tabBarController?.tabBarItem(
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "left-angle-bracket.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(TopicsTableViewController.back(sender:)))
        // let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TopicsTableViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        
        self.hideKeyboardWhenTappedAround()
        
        let underlineAttribute = [kCTUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: contactLabel.text! , attributes: underlineAttribute as [NSAttributedStringKey : Any])
        contactLabel.attributedText = underlineAttributedString
        
        //"Contact Us for Essay Assesment"

    }
    
    @objc func back(sender: UIBarButtonItem) {
        tabBarController?.selectedIndex = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        print("test")
        
        guard MFMailComposeViewController.canSendMail() else {
            print("Mail services are not available")
            return
        }
        
        let toRecipients = ["ielts.writing.assessment@gmail.com"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        
        
        mc.mailComposeDelegate = self
        mc.setToRecipients(toRecipients)
        mc.setSubject(nameField.text!)
        
        mc.setMessageBody("Message:\(String(describing: messageField.text))", isHTML: false)
        
        self.present(mc, animated: true, completion: nil)
        
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("cancelled")
        case MFMailComposeResult.failed.rawValue:
            print("failed")
        case MFMailComposeResult.saved.rawValue:
            print("saved")
        case MFMailComposeResult.sent.rawValue:
            print("sent")
        default:
            break
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension AssessmentViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AssessmentViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
