//
//  VocabularyViewController.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/20/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import UIKit
import Firebase

class VocabularyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var db: Firestore!
    var vocabArray = [Vocabulary]()
    var essayTopic = ""
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // call firestore
        
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
        addVocab()
        getCollection()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return vocabArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! VocabularyTableViewCell
        cell.firstLabel.text = vocabArray[indexPath.row].first
        cell.secondLabel.text = vocabArray[indexPath.row].second

        
        return cell
    }
    
    private func addVocab() {
        
        var ref: DocumentReference? = nil
        ref = db.collection("topicVocabulary").addDocument(data: [
            "topic": "Art",
            "vocab1": "a realistic picture",
            "vocab2": "voice of wisdom"
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
        
    }
    
    
    private func getCollection() {
        // [START get_collection]
     
        activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        activityIndicator.startAnimating()
        
        db.collection("topicVocabulary").whereField("topic", isEqualTo: essayTopic).getDocuments() { (snapshot, error) in
            if  error != nil {
                print("Error getting documents: \(String(describing: error))")
            } else {
                for document in snapshot!.documents {
                    
                    if let vocab1 = document.data()["vocab1"] as? String, let vocab2 = document.data()["vocab2"] as? String
                    {
                        //print(name)
                        print("vvvvv \(vocab1)")
                        let item = Vocabulary(firstWord: vocab1, secondWord: vocab2)
                        self.vocabArray.append(item)
                    
                    }
                    
                }
                
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidesWhenStopped = true
           
            }
        }
    }

    

}
