//
//  Vocabulary.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/20/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import Foundation
class Vocabulary {
    
    let first : String
    let second : String
    
    init(firstWord : String, secondWord : String) {
        first = firstWord
        second = secondWord
    }
    
}
