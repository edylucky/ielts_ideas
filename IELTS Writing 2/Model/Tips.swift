//
//  Tips.swift
//  IELTS Writing 2
//
//  Created by Edy on 9/30/18.
//  Copyright © 2018 Edy. All rights reserved.
//

import Foundation
class Tips {
    
     let tipName : String
     let detail1 : String
     let detail2 : String
     let detail3 : String
     let detail4 : String
     let detail5 : String
     let detail6 : String
     let detail7 : String
     let detail8 : String
     let detail9 : String
     let detail10 : String
     let detail11 : String
     let detail12 : String
    
    init(p_tipName : String, p_detail1 : String, p_detail2 : String, p_detail3 : String, p_detail4 : String, p_detail5 : String, p_detail6 : String, p_detail7 : String, p_detail8 : String, p_detail9 : String, p_detail10 : String, p_detail11 : String, p_detail12 : String) {
        
         tipName = p_tipName
         detail1 = p_detail1
         detail2 = p_detail2
         detail3 = p_detail3
         detail4 = p_detail4
         detail5 = p_detail5
         detail6 = p_detail6
         detail7 = p_detail7
         detail8 = p_detail8
         detail9 = p_detail9
         detail10 = p_detail10
         detail11 = p_detail11
         detail12 = p_detail12
        
    }
    
}
